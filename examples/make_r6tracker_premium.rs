use overwolf_program::{
    kill_overwolf_processes,
    Overwolf,
};

/// TODO: Search each dir for r6tracker
const R6TRACKER_EXTENSION_ID: &str = "ekhcackbfanheaceicpfmhmmeojplojfgkmfnpjo";

fn main() {
    println!("Killing all Overwolf processes...");
    if let Err(e) = kill_overwolf_processes() {
        eprintln!("Failed to kill all Overwolf Processes: {}", e);
        return;
    }

    println!("Initalizing Overwolf Installation API...");
    let mut overwolf = match Overwolf::new() {
        Ok(overwolf) => overwolf,
        Err(e) => {
            eprintln!("Failed to create Overwolf Installation API: {}", e);
            return;
        }
    };

    println!("Locating R6Tracker extension...");
    let installed_extensions = match overwolf.get_installed_extensions() {
        Ok(installed_extensions) => installed_extensions,
        Err(e) => {
            eprintln!("Failed to get installed extensions: {}", e);
            return;
        }
    };

    let r6tracker_extension = match installed_extensions
        .iter()
        .find(|ext| ext.uid == R6TRACKER_EXTENSION_ID)
    {
        Some(r6tracker_extension) => r6tracker_extension,
        None => {
            eprintln!("r6tracker extension is not installed.");
            return;
        }
    };

    let r6tracker_extension_path = overwolf.get_installed_extension_path(&r6tracker_extension);
    if !r6tracker_extension_path.exists() {
        eprintln!("r6tracker extension path does not exist");
        return;
    }

    let js_dir = r6tracker_extension_path.join("js");
    if !js_dir.exists() {
        eprintln!("r6tracker's js dir does not exist");
        return;
    }

    println!("Locating App File...");
    let iter = match std::fs::read_dir(js_dir) {
        Ok(iter) => iter,
        Err(e) => {
            eprintln!("Failed to get next file in dir: {}", e);
            return;
        }
    };

    let app_file = match iter
        .map(|entry| entry.map(|entry| entry.path()))
        .filter(|path| {
            if let Ok(path) = path {
                path.is_file()
            } else {
                true
            }
        })
        .find(|path| match path {
            Ok(path) => path
                .file_name()
                .map_or(false, |name| name.to_string_lossy().starts_with("app")),
            Err(_) => true,
        }) {
        Some(Ok(name)) => name,
        Some(Err(e)) => {
            eprintln!("Failed to iter over files in js dir: {}", e);
            return;
        }
        None => {
            eprintln!("Failed to locate app file.");
            return;
        }
    };

    println!("Updating App File...");
    let app_file_data = match std::fs::read_to_string(&app_file) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("Failed to read app file: {}", e);
            return;
        }
    };

    let new_app_file_data = app_file_data.replace("premiumMember:!1", "premiumMember:!0");

    if let Err(e) = std::fs::write(app_file, new_app_file_data) {
        eprintln!("Failed to write back data to app file: {}", e);
    }

    println!("Done!");
}
