import os
from typing import Optional
import fileinput
import subprocess
import sys

# TODO: Search each dir for r6tracker
R6TRACKER_EXTENSION_ID = "ekhcackbfanheaceicpfmhmmeojplojfgkmfnpjo"
# TODO: Locate highest version/located current running version?
R6TRACKER_CURRENT_VERSION = "2.2.17"

class Overwolf:
    """
    An Api to interact with an Overwolf installation
    """
    
    def __init__(self):
        self.local_app_data_path = os.getenv("LOCALAPPDATA")
        if self.local_app_data_path is None:
            raise OverflowError("Missing LOCALAPPDATA dir")
            
        self.local_data_path = os.path.join(self.local_app_data_path, "Overwolf")
        if not os.path.exists(self.local_data_path):
            raise OverwolfError("Missing Overwolf Local Data Path: " + self.local_data_path)
        
        self.extensions_path = extensions_path = os.path.join(self.local_data_path, "Extensions")
        if not os.path.exists(self.extensions_path):
            raise OverflowError("Missing Overwolf Extensions Path: " + self.extensions_path)
        
    
class OverwolfError:
    def __init__(self, msg=None):
        self.msg = msg
        
def main():
    try:
        print("Initalizing Overwolf Interface...")
        overwolf = Overwolf()
    except OverwolfError as err:
        print(err.msg)
        return
    
    overwolf_extensions_path = overwolf.extensions_path
    r6tracker_extension_dir = os.path.join(overwolf_extensions_path, R6TRACKER_EXTENSION_ID)
    
    if not os.path.exists(r6tracker_extension_dir):
        print("Missing R6Tracker extension dir: " + r6tracker_extension_dir)
        return
    print("Located R6Tracker extension dir: " + r6tracker_extension_dir)
    
    r6tracker_current_extension_dir = os.path.join(r6tracker_extension_dir, R6TRACKER_CURRENT_VERSION)
    
    if not os.path.exists(r6tracker_current_extension_dir):
        print("Missing R6Tracker current extension version dir: " + r6tracker_current_extension_dir)
        return
    print("Located R6Tracker current extension dir: " + r6tracker_current_extension_dir)
    
    r6tracker_js_dir = os.path.join(r6tracker_current_extension_dir, "js")
    
    if not os.path.exists(r6tracker_js_dir):
        print("Missing R6Tracker js dir: " + r6tracker_js_dir)
        return
    print("Located R6Tracker js dir: " + r6tracker_js_dir)
    
    # TODO: Run on all files
    print("Locating App File")
    app_file: Optional[str] = None
    for filename in os.listdir(r6tracker_js_dir):
        print("Found File: " + filename)
        if filename.startswith("app"):
            app_file = os.path.join(r6tracker_js_dir, filename)
            print("Located App File: " + app_file)
            break
            
    if app_file is None:
        print("Missing App File")
        return
        
    file = open(app_file, "r")
    app_file_data = file.read()

    app_file_data = app_file_data.replace("premiumMember:!1", "premiumMember:!0")

    file = open(app_file, "w")
    file.write(app_file_data)
    
    print("Done.")
        
main()