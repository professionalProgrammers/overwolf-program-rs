pub use leveldb::Db;
pub use skylight::{
    processthreadsapi::{
        Process,
        ProcessAccessRights,
    },
    tlhelp32::{
        Snapshot,
        SnapshotFlags,
    },
};
use std::{
    collections::HashMap,
    path::PathBuf,
};

/// Overwolf Process Names
pub const OVERWOLF_PROCESS_NAMES: &[&str] = &[
    "Overwolf.exe",
    "OverwolfBrowser.exe",
    "OverwolfHelper.exe",
    "OverwolfHelper64.exe",
    "OverwolfStore.exe",
];

/// Error type
pub type Result<T> = std::result::Result<T, Error>;

/// Error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Missing Env Var
    #[error("missing env '{0}'")]
    MissingEnv(&'static str),

    /// Missing Path
    #[error("missing path '{0}'")]
    MissingPath(PathBuf),

    /// Level DB Error
    #[error("{0}")]
    LevelDb(leveldb::String),

    /// Non UTF8 Path
    #[error("path is not utf8")]
    PathIsNotUtf8,

    /// Missing LevelDB Value
    #[error("Missing LevelDb Value '{0}'")]
    MissingLevelDbValue(&'static str),

    /// LevelDb Value is not UTF8
    #[error("LevelDb Value Is Not Utf8")]
    LevelDbValueIsNotUtf8(std::str::Utf8Error),

    /// JSON Error
    #[error("{0}")]
    Json(#[from] serde_json::Error),
}

/// Overwolf Installation
#[derive(Debug)]
pub struct Overwolf {
    /// Local App Data Path
    pub local_app_data_path: PathBuf,

    /// Overwolf Local Data Path
    pub local_data_path: PathBuf,

    /// Extensions Path
    pub extensions_path: PathBuf,

    /// Settings Path
    pub settings_path: PathBuf,

    /// Settings DB
    pub settings_db: Db,
}

impl Overwolf {
    /// Make a new Overwolf installation
    pub fn new() -> Result<Self> {
        let local_app_data_path: PathBuf = std::env::var_os("LOCALAPPDATA")
            .ok_or(Error::MissingEnv("LOCALAPPDATA"))?
            .into();

        let local_data_path = local_app_data_path.join("Overwolf");
        if !local_app_data_path.exists() {
            return Err(Error::MissingPath(local_data_path));
        }

        let extensions_path = local_data_path.join("Extensions");
        if !extensions_path.exists() {
            return Err(Error::MissingPath(extensions_path));
        }

        let settings_path = local_data_path.join("Settings");
        if !settings_path.exists() {
            return Err(Error::MissingPath(settings_path));
        }

        let settings_db_path = settings_path.join("settings.db");
        let settings_db = Db::open(
            settings_db_path.to_str().ok_or(Error::PathIsNotUtf8)?,
            Default::default(),
        )
        .map_err(Error::LevelDb)?;

        Ok(Overwolf {
            local_app_data_path,
            local_data_path,
            extensions_path,
            settings_path,

            settings_db,
        })
    }

    /// Get installed extensions
    pub fn get_installed_extensions(&mut self) -> Result<Vec<InstalledExtension>> {
        let value = self
            .settings_db
            .get(&Default::default(), "extensions.installed".as_bytes())
            .map_err(Error::LevelDb)?
            .ok_or(Error::MissingLevelDbValue("extensions.installed"))?;

        let value = value.to_str().map_err(Error::LevelDbValueIsNotUtf8)?;

        Ok(serde_json::from_str(value)?)
    }

    /// Get an installed extension path. Does not check if it exists.
    pub fn get_installed_extension_path(
        &self,
        installed_extension: &InstalledExtension,
    ) -> PathBuf {
        self.extensions_path
            .join(&installed_extension.uid)
            .join(&installed_extension.version_data.version)
    }
}

/// Installed Extension
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct InstalledExtension {
    /// Identifier
    #[serde(rename = "UID")]
    pub uid: String,

    /// Whether the extension is disabled.
    #[serde(rename = "IsDisabled")]
    pub is_disabled: bool,

    /// Version Data
    #[serde(rename = "VersionData")]
    pub version_data: VersionData,

    /// Force Update
    #[serde(rename = "ForceUpdate")]
    pub force_update: bool,

    /// Unknown K/Vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Version Data for an extension
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct VersionData {
    /// Extension Version
    #[serde(rename = "Version")]
    pub version: String,

    /// Unknown K/Vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Kill current Overwolf processes
pub fn kill_overwolf_processes() -> std::io::Result<()> {
    let mut snapshot = Snapshot::new(SnapshotFlags::SNAP_ALL)?;
    let processes = snapshot
        .iter_processes()
        .filter(|process| {
            let process_exe_name = process.exe_name();
            OVERWOLF_PROCESS_NAMES
                .iter()
                .any(|name| *name == process_exe_name)
        })
        .map(|process_info| {
            Process::open(
                ProcessAccessRights::TERMINATE | ProcessAccessRights::SYNCHRONIZE,
                process_info.pid(),
            )
        })
        .collect::<std::io::Result<Vec<_>>>()?;

    for process in processes {
        // Killing some processes terminates others, which i think causes an access denied error if a double termination is attempted?
        // Allow this call to fail.
        let _ = process.terminate(1).is_ok();
        process.wait(60 * 1000)?;
    }

    Ok(())
}
