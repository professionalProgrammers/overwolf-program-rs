# overwolf-program-rs 
[![Build status](https://ci.appveyor.com/api/projects/status/8ibpselaey1p2172?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/overwolf-program-rs)
[![](https://tokei.rs/b1/bitbucket.org/professionalProgrammers/overwolf-program-rs)](https://bitbucket.org/professionalProgrammers/overwolf-program-rs)

This is an API for accessing an installed Overwolf's App data. See the examples folder for usage. This library only works on Windows.

## Testing
There are currently no tests. You can help by writing some.